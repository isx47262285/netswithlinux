EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.112/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt11
/system backup save name="20170317_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

Reseteamos la configuracion i cargamos el backup que tenemos guardado previamente,
los ficheros que tenemos guardados en el mikortik se encuentran en /file print.

		[admin@MikroTik] > system reset-configuration
		Dangerous! Reset anyway? [y/N]: y
		
		/system reset-configuration
		/system backup load name=20170317_zeroconf.backup 
```
[admin@mkt11] > /export 
# jan/02/1970 00:00:37 by RouterOS 6.33.5
# software id = KHNI-GJGJ
#
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    distance=indoors frequency=auto mode=ap-bridge ssid=MikroTik-C38993 \
    wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether1 ] name=eth1
set [ find default-name=ether2 ] name=eth2
set [ find default-name=ether3 ] name=eth3
set [ find default-name=ether4 ] name=eth4
/ip neighbor discovery
set eth1 discover=no
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=\
    192.168.88.0
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept establieshed,related" \
    connection-state=established,related
add action=drop chain=input comment="defconf: drop all from WAN" \
    in-interface=eth1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" \
    connection-state=invalid
add action=drop chain=forward comment=\
    "defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat \
    connection-state=new in-interface=eth1
/system identity
set name=mkt11
/system routerboard settings
set cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes
add
```

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*112: red pública
*212: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 name=wFree ssid="free112"
/interface wireless add ssid="private212" name=wPrivate master-interface=wFree
```

###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

Haciendo interface wireless enabled/disabled activamos o deshabilitamos la wifi

/interface wireless enabled
/interface wireless disabled

[admin@mkt12] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS 
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C3:89:8F
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C3:89:90
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:C3:89:91
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C3:89:92
 4  X  wFree                               wlan             1500  1600       2290 6C:3B:6B:C3:89:93
 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C3:89:93

[admin@mkt12] > interface wireless set wifi1 name=wFree 
[admin@mkt12] > interface wireless set wlan2 name=wPrivate 


 Para deshabilitar una interficie lo haremos mediante la letra X

## Crear interfaces virtuales y hacer bridges

```
creamos las vlans apartir de la eth4

/interface vlan add name eth4-vlan112 vlan-id=112 interface=eth4
/interface vlan add name eth4-vlan212 vlan-id=212 interface=eth4   
#/interface vlan add name eth4-vlan112 vlan-id=112 interface=eth3 **** duda!!!(solo añadiendo la etiqueta de la vlan parece suficiente)


/interface bridge add name=br-vlan112
/interface bridge add name=br-vlan212

# añadimos los bridges a la interface la eth4, eth3 y la wlan-free 
# para poder tener conectividad entre los puertos y se puedan hacer pings..... 

/interface bridge port add interface=eth4-vlan112 bridge=br-vlan112
/interface bridge port add interface=eth3 bridge=br-vlan112
/interface bridge port add interface=wFree  bridge=br-vlan112     


/interface bridge port add interface=eth4-vlan211 bridge=br-vlan211
/interface bridge port add interface=wPrivate   bridge=br-vlan211
/interface print
```

###[ejercicio3] Comenta cada una de estas líneas de la configuración6 
anterior para que quede bien documentado



[admin@mkt12] > /interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C3:89:8F
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C3:89:90
 2   S eth3                                ether            1500  1598       2028 6C:3B:6B:C3:89:91
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C3:89:92
 4  XS wFree                               wlan             1500  1600       2290 6C:3B:6B:C3:89:93
 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C3:89:93
 6  R  br-vlan112                          bridge           1500  1594            6C:3B:6B:C3:89:92
 7  R  br-vlan212                          bridge           1500  1594            6C:3B:6B:C3:89:92
 8   S eth4-vlan112                        vlan             1500  1594            6C:3B:6B:C3:89:92
 9   S eth4-vlan212                        vlan             1500  1594            6C:3B:6B:C3:89:92


#0 interface eth1 
#1 interface eth2 R  que esta runing y es nuestro puerto para la configuracion del router con telnet 192.168.88.1 
#2 interface eth3 
#3 interface eth4 el puerto le hemos añadido 2 vlans con el objetivo de separar las redes privadas de las gratuitas
#4 interface wFree interficie wireless (wifi) gratuita que estamos configurando y esta disabled
#5 interface wPrivate wlan wirelees privada que esta disabled (una es hija de la otra wlan)
#6 br-vlan112 id 112 que esta asociada a la eth4  esta running
#7 br-vlan212 id 212 que esta asociada a la eth4  esta running
#8 vlan112 id 112 que esta asociada a la eth4
#9 vlan212 id 212 que esta asociada a la eth4



### [ejercicio4] Pon una ip 172.17.112.1/24 a br-vlan112 
y 172.17.212.1/24 a br-vlan212 y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración

[admin@mkt12] > /ip address print 
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                                 
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                      
 1   172.17.112.1/24    172.17.112.0    br-vlan112                                                                
 2   172.17.212.1/24    172.17.212.0    br-vlan212                                                                

##como tenemos que hacer pings entre interficies virtuales, tambien tenemos que 
##crear esas interficies en nuestro ordenador con las mismas etiquetas.

[root@j12 ~]# ip a f enp2s0
[root@j12 ~]# ip link add link enp2s0 name v112 type vlan id 112
[root@j12 ~]# ip link add link enp2s0 name v212 type vlan id 212
[root@j12 ~]# ip link set v112 up 
[root@j12 ~]# ip link set v212 up 



6: v112@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 172.17.112.12/24 scope global v112
       valid_lft forever preferred_lft forever
    inet6 fe80::16da:e9ff:fe99:a1bc/64 scope link 
       valid_lft forever preferred_lft forever
7: v212@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 172.17.212.12/24 scope global v212
       valid_lft forever preferred_lft forever
    inet6 fe80::16da:e9ff:fe99:a1bc/64 scope link 
       valid_lft forever preferred_lft forever


## una vez creadas y con las interficies levantadas procedemos a comprovar la conectividad con los pings


[root@j12 ~]# ping 172.17.112.1
PING 172.17.111.1 (172.17.111.1) 56(84) bytes of data.
64 bytes from 172.17.112.1: icmp_seq=1 ttl=64 time=0.426 ms
64 bytes from 172.17.112.1: icmp_seq=2 ttl=64 time=0.277 ms
^C
--- 172.17.111.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.277/0.351/0.426/0.076 ms
[root@j12 ~]# ping 172.17.212.1
PING 172.17.211.1 (172.17.211.1) 56(84) bytes of data.
64 bytes from 172.17.212.1: icmp_seq=1 ttl=64 time=0.466 ms
64 bytes from 172.17.212.1: icmp_seq=2 ttl=64 time=0.293 ms


### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

## primero se crea el pool que seran los rangos del cual nos dara una ip el dhcp-server
## cuando la pidamos automaticamente con dhclient

[admin@mkt12] > ip pool add ranges=172.17.112.101-172.17.112.250 name=range_public
[admin@mkt12] > ip pool add ranges=172.17.212.101-172.17.212.250 name=range_private 

## ponemos una ip al br-vlan porque aqui es nuestro punto de partida (previamente ya teniamos la .1 configurada)
 
[admin@mkt12] > ip address add interface=br-vlan112 address=172.17.112.112 netmask=255.255.255.0
[admin@mkt12] > ip address add interface=br-vlan212 address=172.17.212.212 netmask=255.255.255.0   
[admin@mkt12] > ip address print                                                                
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                            
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                 
 1   172.17.112.1/24    172.17.112.0    br-vlan112                                                           
 2   172.17.212.1/24    172.17.212.0    br-vlan212                                                           
 3   172.17.112.112/24  172.17.112.0    br-vlan112                                                           
 4   172.17.212.212/24  172.17.212.0    br-vlan212   

## procedemos a crear el servidor de dhcp añadiendole el rango de ips que dara de manera automatica.
## primero lo añadimos al br-vlanXXX con el pool 

[admin@mkt12] > ip dhcp-server add interface=br-vlan112 address-pool=range_public 
[admin@mkt12] > ip dhcp-server add interface=br-vlan212 address-pool=range_private  

## segundo creamos el dhcp con la red y todo lo necesario para q funcione

[admin@mkt12] > ip dhcp-server network add gateway=172.17.112.1 dns-server=8.8.8.8 netmask=255.255.255.0 address=172.17.112.0/24 domain=roberto.com   
[admin@mkt12] > ip dhcp-server network add gateway=172.17.212.1 dns-server=8.8.8.8 netmask=255.255.255.0 address=172.17.212.0/24 domain=roberto.com 

### desde el ordenador pedimos una ip automatica para las vlans
[root@j12 ~]# dhclient v212
[root@j12 ~]# dhclient v112


"ojo q si ponemos dhclient -r nos quita ips"

4: v112@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
5: v212@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 172.17.212.250/24 brd 172.17.212.255 scope global dynamic v212
       valid_lft 658sec preferred_lft 658sec



apuntamos el resultado de las modificaciones

[admin@mkt12] /ip dhcp-server network> print
 # ADDRESS            GATEWAY         DNS-SERVER      WINS-SERVER     DOMAIN     
 0 172.17.112.0/24    172.17.112.1    8.8.8.8                         roberto@j12
 1 172.17.212.0/24    172.17.212.1    8.8.8.8                         roberto@j12
[admin@mkt12] /ip dhcp-server network> set 0 domain=roberto.com 
[admin@mkt12] /ip dhcp-server network> set 1 domain=roberto.com                      
[admin@mkt12] /ip dhcp-server> print
Flags: X - disabled, I - invalid 
 #   NAME       INTERFACE     RELAY           ADDRESS-POOL     LEASE-TIME ADD-ARP
 0 X dhcp1      br-vlan112                    range_public     10m       
 1 X dhcp2      br-vlan212                    range_private    10m       

levantamos las interficies de dhcp

[admin@mkt12] /ip dhcp-server> set 1 disabled=no 
[admin@mkt12] /ip dhcp-server> set 0 disabled=no
[admin@mkt12] /ip dhcp-server> print            
Flags: X - disabled, I - invalid 
 #   NAME       INTERFACE     RELAY           ADDRESS-POOL     LEASE-TIME ADD-ARP
 0   dhcp1      br-vlan112                    range_public     10m       
 1   dhcp2      br-vlan212                    range_private    10m       

configuramos la salida a internet de la eth1 que ira hasta la red de la escuela

[admin@mkt12] /ip firewall nat> add action=masquerade chain=srcnat out-interface=eth1

[admin@mkt12] /ip firewall nat> /ip address export 
# mar/28/2017 13:30:37 by RouterOS 6.33.5
# software id = 2WZ5-9PJS
#
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
add address=172.17.112.1/24 interface=br-vlan112 network=172.17.112.0
add address=172.17.212.1/24 interface=br-vlan212 network=172.17.212.0
add address=172.17.112.112/24 interface=br-vlan112 network=172.17.112.0
add address=172.17.212.212/24 interface=br-vlan212 network=172.17.212.0
add address=192.168.3.212/16 interface=eth1 network=192.168.0.0
[admin@mkt12] /ip firewall nat> 

###########################################################################################
[root@j12 ~]# dhclient v112
[root@j12 ~]# ip r
default via 172.17.112.1 dev v112 
172.17.112.0/24 dev v112  proto kernel  scope link  src 172.17.112.250 
192.168.88.0/24 dev enp0s29u1u4  proto kernel  scope link  src 192.168.88.12 
[root@j12 ~]# cat /etc/resolv.conf 
; generated by /usr/sbin/dhclient-script
search roberto.com informatica.escoladeltreball.org
nameserver 8.8.8.8
[root@j12 ~]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
.64 bytes from 8.8.8.8: icmp_seq=1 ttl=45 time=31.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=45 time=31.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=45 time=31.4 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 31.410/31.452/31.521/0.049 ms
[root@j12 ~]# ping www.google.com
PING www.google.com (216.58.211.196) 56(84) bytes of data.
64 bytes from mad01s25-in-f4.1e100.net (216.58.211.196): icmp_seq=1 ttl=53 time=12.2 ms
64 bytes from mad01s25-in-f4.1e100.net (216.58.211.196): icmp_seq=2 ttl=53 time=12.0 ms
64 bytes from mad01s25-in-f4.1e100.net (216.58.211.196): icmp_seq=3 ttl=53 time=12.0 ms
^C
--- www.google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 12.077/12.135/12.242/0.147 ms
[root@j12 ~]# 



[admin@mkt12] > ip firewall nat add action=masquerade chain=srcnat out-interface=eth1
[admin@mkt12] > ping 8.8.8.8
  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                                                   
    0 8.8.8.8                                    56  56 11ms 
    1 8.8.8.8                                    56  56 11ms 
    2 8.8.8.8                                    56  56 11ms 
    sent=3 received=3 packet-loss=0% min-rtt=11ms avg-rtt=11ms max-rtt=11ms 


#ultimo backup 
[admin@mkt12] > system backup save name="20170330_dhcp_ping_masquerade_ok"
Saving system configuration
Configuration backup saved
[admin@mkt12] > 

#LISTAMOS LAS WIFIS EN EL ROUTER LAS ACTIVAMOS Y CONECTAMOS
[admin@mkt12] > interface wireless print 
Flags: X - disabled, R - running 
 0 X  name="wfree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:F2 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="free112" frequency=auto 
      band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none 
      wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
      security-profile=default compression=no 

 1 X  name="wprivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:F2 arp=enabled interface-type=virtual-AP master-interface=wfree ssid="private212" vlan-mode=no-tag 
      vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes 
      default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default 

[admin@mkt12] > interface wireless set 0 disabled=no 
[admin@mkt12] > interface wireless set 1 disabled=no 

[admin@mkt12] > interface wireless print            
Flags: X - disabled, R - running 
 0    name="wfree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:F2 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="free112" frequency=auto 
      band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none 
      wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
      security-profile=default compression=no 

 1    name="wprivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:F2 arp=enabled interface-type=virtual-AP master-interface=wfree ssid="private212" vlan-mode=no-tag 
      vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes 
      default-ap-tx-limit=0 default-client-tx-limit=0 hid
      
#LA INTERFAZ WIRELLES V112 ESTA RUNNING Y CONECTAMOS

      
      [admin@mkt12] > interface wireless print
Flags: X - disabled, R - running 
 0  R name="wfree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:F2 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="free112" frequency=auto 
      band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none 
      wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
      security-profile=default compression=no 



#comprovamos que la wifi la tenemos disponible la red y nos podemos conectar

[admin@mkt12] /interface wireless> registration-table print 
 # INTERFACE                                                                        RADIO-NAME       MAC-ADDRESS       AP  SIGNAL-STRENGTH TX-RATE UPTIME              
 0 wfree                                                                                             8C:EB:C6:07:C3:6F no  -49dBm@6Mbps    54Mbps  2m33s 


### [ejercicio6] Activar redes wifi y dar seguridad wpa2


[admin@mkt12] /interface wireless> security-profiles print         
Flags: * - default 
 0 * name="default" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="" 
     supplicant-identity="MikroTik" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password="" 
     static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3="" 
     static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no 
     interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
     management-protection-key="" 
[admin@mkt12] /interface wireless> security-profiles add name=profile-private

[admin@mkt12] /interface wireless> security-profiles edit 1 wpa 
no such item
[admin@mkt12] /interface wireless> security-profiles print                   
Flags: * - default 
 0 * name="default" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="" 
     supplicant-identity="MikroTik" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password="" 
     static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3="" 
     static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no 
     interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
     management-protection-key="" 

 1   name="profile-private" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="" 
     supplicant-identity="mkt12" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password="" static-algo-0=none 
     static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3="" static-transmit-key=key-0 
     static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no interim-update=0s 
     radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
     management-protection-key="" 
[admin@mkt12] /interface wireless> security-profiles edit 1 wpa2-pre-shared-key 
[admin@mkt12] /interface wireless> security-profiles print                      
Flags: * - default 
 0 * name="default" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="" 
     supplicant-identity="MikroTik" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password="" 
     static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3="" 
     static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no 
     interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
     management-protection-key="" 

 1   name="profile-private" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="profile-private" 
     supplicant-identity="mkt12" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password="" static-algo-0=none 
     static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3="" static-transmit-key=key-0 
     static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no interim-update=0s 
     radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
     management-protection-key="" 
[admin@mkt12] /interface wireless> security-profiles set 1 authentication-types=wpa
wpa-eap  wpa-psk  wpa2-eap  wpa2-psk
[admin@mkt12] /interface wireless> security-profiles set 1 authentication-types=wpa2-psk 
[admin@mkt12] /interface wireless> security-profiles set 1 mode=static-keys-required 
[admin@mkt12] /interface wireless> 

[admin@mkt12] > system backup save name="20170330_wifi_free_okey"
Saving system configuration
Configuration backup saved

### [ejercicio6b] Opcional (monar portal cautivo)


### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

[admin@mkt12] /ip firewall> export
# apr/06/2017 09:32:34 by RouterOS 6.33.5
# software id = 2WZ5-9PJS
#
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add action=drop chain=forward comment="defconf: DROP ICMP V112 V212" dst-address=172.17.212.0/24 protocol=icmp src-address=172.17.112.0/24
add chain=input comment="defconf: accept ssh on port 22340" dst-port=22340 protocol=tcp
add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=eth1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
add chain=forward comment="defconf: accept established,related" connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new in-interface=eth1
/ip firewall nat
add action=masquerade chain=srcnat out-interface=eth1


[root@j12 ~]# ping 172.17.212.250
PING 172.17.212.250 (172.17.212.250) 56(84) bytes of data.
^C
--- 172.17.212.250 ping statistics ---
12 packets transmitted, 0 received, 100% packet loss, time 10999ms

[root@j12 ~]# 


[admin@mkt12] > system backup save name="20170405_firewall_okey"
Saving system configuration
Configuration backup saved


### [ejercicio7 B]  SEGURIZAR EL ROUTER Y CAMBIAR PUERTOS DE ESCUCHA SSH


## Script para actualizar la ip dinámica

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

