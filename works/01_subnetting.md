# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.16.4.0 | 172.16.4.127 | .1 a .126
174.187.55.6/23 |  174.187.54.0 | 174.187.55.255  | 174.187.54.1 a 174.187.55.254
10.0.25.253/18 | 10.0.0.0 | 10.0.63.255 | 10.0.0.1 a 10.0.63.254
209.165.201.30/27 | 209.165.201.0/27 |209.165.201.31 |209.165.201.1 a 209.165.201.30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

**Amb /23 tinc 510 hosts**. Justificació:
* 32 - 23 = 9 bits per host
* 2^9 = 512 combinacions posibles
* 512 - 2 adreçes reservades = 510 hosts

De /16 a /23 hi han 7 bits per fer combinacions de subxarxes. 2^7 = 128 combinacions = 128 subxarxes posibles

La màscara CIDR /23 en format binari és: 

    11111111.11111111.11111110.00000000
    
i en format decimal es: 

    255.255.254.0 


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

    10.192.0.0/21 sera mascara para 1500 usuarios
    o tambien podemos definir la mascara asi :
    255.255.248.0 = 21    11111111.11111111.11111 000.00000000

b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?
    
    si definimos /10 tenemos 22bits para host que nos dara 2^22 = 4194304
    host
    si queremos tener redes de 1500 dispositivos  tendriamos que marcar un
    minimo de /21 para alcanzar los 1500 dispositivos
    
    el / 21 nos marcaria 2^11 = 2048 host 
    
    tendriamos que pasar de un /10 a / 21 para tener una subred de 2048 y
    nos caben los 1500 dispositivos que necesitamos 
    
    por tanto entre /10  y / 21 hay 11 bits de diferencia y podemos
    concluir que 2^11 = 2048 subredes posibles de 2048 host cada una.

c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

    10.192.0.0/21 nos perminte 2046 host si la queremos pasar a una red
    que nos soporte 500 host tendriamos q definir la nueva red ha de ser
    
    10.192.0.0/23 que nos dara un maximo de 510 host cada una.
    
    por tanto entre el /21 y /23 hay 2 bits de diferencia para subred que
    seran 2^2 = 4  posibles subredes de 510 cada una.
    
#### Network:   10.192.0.0/23     |    00001010.11000000.0000000 0.00000000

Broadcast: 10.192.1.255     |    00001010.11000000.0000000 1.11111111

HostMin:   10.192.0.1       |    00001010.11000000.0000000 0.00000001

HostMax:   10.192.1.254     |    00001010.11000000.0000000 1.11111110

Hosts/Net: 510                   


#### network 10.192.2.0/23         00001010.11000000.0000001 0.00000000 

Broadcast: 10.192.3.255     ||     00001010.11000000.0000001 1.11111111

HostMin:   10.192.2.1         ||   00001010.11000000.0000001 0.00000001

HostMax:   10.192.3.254         || 00001010.11000000.0000001 1.11111110

Hosts/Net: 510                   

#### Network:   10.192.4.0/23     ||    00001010.11000000.0000010 0.00000000

Broadcast: 10.192.5.255       ||   00001010.11000000.0000010 1.11111111

HostMin:   10.192.4.1       ||     00001010.11000000.0000010 0.00000001

HostMax:   10.192.5.254     ||     00001010.11000000.0000010 1.11111110

Hosts/Net: 510                  


#### Network:   10.192.6.0/23         00001010.11000000.0000011 0.00000000 
Broadcast: 10.192.7.255  ||        00001010.11000000.0000011 1.11111111

HostMin:   10.192.6.1      ||      00001010.11000000.0000011 0.00000001

HostMax:   10.192.7.254 ||         00001010.11000000.0000011 1.11111110

Hosts/Net: 510                   (Private Internet)




##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius

a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

    10.128.0.0/20  
    MASK 255.255.240.0  = 11111111.11111111.11110000.00000000
    
    con esta red conseguimos un maximo de 4092 host asi que cumplimos el
    requisito de tener 3250 dispositivos.

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

    Para cumplir la condicion de tener 650 dispositivos tenemos que pasar
    de un /20 a un /22 y con eso conseguiremos un totalidad de 2² = 4
    subredes posibles pero con esto no conseguimos tener las 5 subredes
    necesarias.
    
    por otra parte podemos conseguir tener 5 subredes  pasando de tener 
    /20 a un /23  y con esto hay 2³ = 8 subredes posibles y cumplir las
    condiciones de subredes, sin embargo no cumplimos la condicion de 650
    dispositivos en cada red.
    
    conclusion debemos prescindir de una de las condiciones para poder
    adaptar una red que satisfaga las necesidades, pero las dos juntas no
    pueden ser posibles.
    
    

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

    si queremos 5 subredes tenemos que pasar de /20 a /23 por tanto:
    
    20-23 = 3 (restamos la cantidad de bits actuales con los que queremos conseguir ) 
    2³ = 8 subredes 
    
    si tenemos un /23 definimos:
    
    32-23 = 9 bits para los host 
    2⁹ = 512  
    512 - 2 (broad y red) = 510 host por cada subred.


d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

    partiendo de 10.128.0.0/20 
    tenemos que podemos tener un maximo de 4094 host
    
    si podemos tener 2 subredes de 1625 dispositivos cada una.
    
    queremos dividir esta red en dos subredes:
    
    pasamos de / 20   a / 21  y tenemos 
    21 -20 = 1
    2¹ = 2 subredes posibles 
    
    32 - 21 = 11 
    2¹¹= 2048 host  para cumplir con 1625 host 
    

##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?


    se necesitan 3bits para poder tener 6 subredes :
    
    2³= 8 subredes posibles 
    
    por tanto pasariamos de /12 a /15 

b) Dóna la nova MX, tant en format decimal com en binari.

    255.254.0.0 = 15  
    11111111.11111110.00000000.00000000 = 15


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 |172.16.0.0/15 |172.17.255.255  |172.16.0.1  a 172.17.255.254  
Xarxa 2 |172.18.0.0/15 |  172.19.255.255   | 172.18.0.1   a  172.19.255.254 
Xarxa 3 |172.20.0.0/15  |172.21.255.255   |172.20.0.1  a 172.21.255.254 
Xarxa 4 |172.22.0.0/15   |172.23.255.255   |172.22.0.1 a 172.23.255.254  
Xarxa 5 |172.24.0.0/15  |172.25.255.255 |172.24.0.1  a  172.25.255.254 
Xarxa 6 |172.26.0.0/15  |172.27.255.255 |172.26.0.1 a  172.27.255.254  


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

    /12 a / 15 hay 3 bits de diferencia
    
    por tanto :
    
    15-12 = 3 
    2³ = 8 subredes posibles 
    
    
e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

    con un /12 :
    32 - 12 = 20 bits para host 
    
    2²⁰= 1048576 host ( hay que restar el broadcast y la red)
    
    y con el / 15 
    
    32 -15 = 17 bits para los hots 
    
    2¹⁷= 131072  hots posibles (hay que restar el broadcast y la red)
    
    por cada subred (6 las que se piden y 8 el maximo de subredes posibles)

##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 4 subxarxes?
    
    se necesitan 2 bits para crear 4 subredes :
    2² = 4 
    
    por tanto pasamos de un /24 a un /26

b) Dóna la nova MX, tant en format decimal com en binari.
    
     255.255.255.192 = 26  11111111.11111111.11111111.11 000000

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1| 192.168.1.0/26 11000000.10101000.00000001.00 000000 |192.168.1.63          11000000.10101000.00000001.00 111111 |192.168.1.1  a 192.168.1.62
Xarxa 2| 192.168.1.64/26  |  192.168.1.127   |192.168.1.65   a 192.168.1.126
Xarxa 3| 192.168.1.128/26  |192.168.1.191  |192.168.1.129   192.168.1.190
Xarxa 4|192.168.1.192/26  |192.168.1.255  |192.168.1.193    a 192.168.1.254


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

    si queremos hacer mas de 4 subredes tendriamos que ocupar un bit mas 
    
    porque 2²= 4 subredes 
    
    y 2³ = 8 subredes  
    
    
e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

        la mascara sera 
        
        111111111.11111111.11111111.11000000
        225.255.255.192

        26 - 24 = 2
        2²=4 subredes 
        
        y cada subred : 
        
        32-26 = 6 
        2⁶= 64 host por cada subred 
        
        
