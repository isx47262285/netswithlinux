#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
	ip r f all 
	ip a f dev enp2s0

	comprovamos con 
	ip r
	ip a
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff


Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	$ ip a a 2.2.2.2/24	 dev enp2s0
	$ ip a a 3.3.3.3/16  dev enp2s0
	$ ip a a 4.4.4.4/25  dev enp2s0
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 2.2.2.2/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp2s0
       valid_lft forever preferred_lft forever

	

Consulta la tabla de rutas de tu equipo

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132


PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.033 ms
64 bytes from 2.2.2.2: icmp_seq=2 ttl=64 time=0.040 ms
64 bytes from 2.2.2.2: icmp_seq=3 ttl=64 time=0.039 ms
64 bytes from 2.2.2.2: icmp_seq=4 ttl=64 time=0.039 ms
64 bytes from 2.2.2.2: icmp_seq=5 ttl=64 time=0.042 ms
^C
--- 2.2.2.2 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 3999ms
rtt min/avg/max/mdev = 0.033/0.038/0.042/0.007 ms

[root@j12 netswithlinux]# ping 2.2.2.254 
PING 2.2.2.254 (2.2.2.254) 56(84) bytes of data.
From 2.2.2.2 icmp_seq=1 Destination Host Unreachable
From 2.2.2.2 icmp_seq=2 Destination Host Unreachable
From 2.2.2.2 icmp_seq=3 Destination Host Unreachable
From 2.2.2.2 icmp_seq=4 Destination Host Unreachable
From 2.2.2.2 icmp_seq=5 Destination Host Unreachable
From 2.2.2.2 icmp_seq=6 Destination Host Unreachable
From 2.2.2.2 icmp_seq=7 Destination Host Unreachable
From 2.2.2.2 icmp_seq=8 Destination Host Unreachable
^C
--- 2.2.2.254 ping statistics ---
8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms
pipe 4

	nos dice que el host es inalcanzable porque apesar de estar en la misma red 
	no esta conectado y no recibe los paquetes.
	
	[root@j12 netswithlinux]# ping 2.2.5.2
	connect: Network is unreachable
	
	directamente nos dice que no podemos conectarnos a la ip porque no es de nuestra red
	la mascara que tenemos es una /24 y la ip 2.2.5.2 no pertecene a ninguna de nuestras redes.


# ping 3.3.3.35 
PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
From 3.3.3.3 icmp_seq=2 Destination Host Unreachable
From 3.3.3.3 icmp_seq=3 Destination Host Unreachable
From 3.3.3.3 icmp_seq=4 Destination Host Unreachable
From 3.3.3.3 icmp_seq=5 Destination Host Unreachable
From 3.3.3.3 icmp_seq=6 Destination Host Unreachable
From 3.3.3.3 icmp_seq=7 Destination Host Unreachable
From 3.3.3.3 icmp_seq=8 Destination Host Unreachable
^C
--- 3.3.3.35 ping statistics ---
11 packets transmitted, 0 received, +8 errors, 100% packet loss, time 10001ms
pipe 4
 
	pertenecemos a la red  pero el host .35 no esta conectado o no tiene esta asociada
	a un ordenador.
	
	
# ping 3.3.200.45
PING 3.3.200.45 (3.3.200.45) 56(84) bytes of data.
From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
From 3.3.3.3 icmp_seq=2 Destination Host Unreachable
From 3.3.3.3 icmp_seq=3 Destination Host Unreachable
From 3.3.3.3 icmp_seq=4 Destination Host Unreachable
^C
--- 3.3.200.45 ping statistics ---
5 packets transmitted, 0 received, +4 errors, 100% packet loss, time 4000ms
pipe 4

	pertecencemos a la red pero no podemos hacer una conexion de red 
	porque el host .200.45 no esta conectado o asociado aun ordenador.
	
	
# ping4.4.4.8
bash: ping4.4.4.8: command not found...
[root@j12 netswithlinux]# ping 4.4.4.8
PING 4.4.4.8 (4.4.4.8) 56(84) bytes of data.
From 4.4.4.4 icmp_seq=1 Destination Host Unreachable
From 4.4.4.4 icmp_seq=2 Destination Host Unreachable
From 4.4.4.4 icmp_seq=3 Destination Host Unreachable
From 4.4.4.4 icmp_seq=4 Destination Host Unreachable
^C
--- 4.4.4.8 ping statistics ---
5 packets transmitted, 0 received, +4 errors, 100% packet loss, time 4001ms
pipe 4


	igual que el anterior tenemos una ip de red pero el host .4.8  no esta conectado 
	no alcanza la conexion.



# ping 4.4.4.132
connect: Network is unreachable
	no pertenecemos al a misma red.....  la mascara / 25 nos limita desde el host .1 al .126


#### 2. ip lin


Borra todas las rutas y direcciones ip de la tarjeta ethernet

Conecta una segunda interfaz de red por el puerto usb

2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1c:2d brd ff:ff:ff:ff:ff:ff

Cambiale el nombre a usb0

#ip link set usb0 down

# ip link set usb0 name usbwlan



comprovamos 
ip a
[...]
3: usbwlan: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1c:2d brd ff:ff:ff:ff:ff:ff





Modifica la dirección MAC

# ip link set usbwlan address 00:11:22:33:44:55
# ip link set usbwlan up


3: usbwlan: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff


Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.
ip a a 5.5.5.5/24 dev usbwlan 
 ip a a 7.7.7.7/24 dev enp2s0


comprovamos 

ip a



2: enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 7.7.7.7/24 scope global enp2s0
       valid_lft forever preferred_lft forever
3: usbwlan: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
    inet 5.5.5.5/24 scope global usbwlan
       valid_lft forever preferred_lft forever

Observa la tabla de rutas
5.5.5.0/24 dev usbwlan  proto kernel  scope link  src 5.5.5.5 linkdown 

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

ip a a 172.16.99.12/24 dev enp2s0

2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 172.16.99.12/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet6 fe80::96de:80ff:fe49:7d9f/64 scope link 
       valid_lft forever preferred_lft forever

Lanzar iperf en modo servidor en cada ordenador
iperf -s  ( es la orden para hacer de servidor)
iperf -c  172.16.99.12 (es para el cliente )

Comprueba con netstat en qué puerto escucha

# netstat -tlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      4234/iperf          
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      860/rpcbind         
tcp        0      0 0.0.0.0:60721           0.0.0.0:*               LISTEN      858/rpc.statd       
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1452/cupsd          
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      703/postgres        
tcp6       0      0 :::47019                :::*                    LISTEN      858/rpc.statd       
tcp6       0      0 :::111                  :::*                    LISTEN      860/rpcbind         
tcp6       0      0 ::1:631                 :::*                    LISTEN      1452/cupsd          
tcp6       0      0 ::1:5432                :::*                    LISTEN      703/postgres        


	estamos en escucha desde el puerto 5001

Conectarse desde otro pc como cliente

	iperf -c 172.16.99.11



Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

	# tshark -i enp2s0 -w /var/tmp/captura.pcap -c30 


Encontrar los 3 paquetes del handshake de tcp

# tshark -r captura.pcap -x -Y "frame.number==7"
Running as user "root" and group "root". This could be dangerous.
0000  94 de 80 49 7d 9f 14 da e9 99 a1 bc 08 00 45 00   ...I}.........E.
0010  00 3c 78 cd 40 00 40 06 a3 b6 ac 10 63 0b ac 10   .<x.@.@.....c...
0020  63 0c b8 3e 13 89 90 1f aa ad 00 00 00 00 a0 02   c..>............
0030  72 10 de b1 00 00 02 04 05 b4 04 02 08 0a 00 10   r...............
0040  d2 60 00 00 00 00 01 03 03 07                     .`........

# tshark -r captura.pcap -x -Y "frame.number==8"
Running as user "root" and group "root". This could be dangerous.
0000  14 da e9 99 a1 bc 94 de 80 49 7d 9f 08 00 45 00   .........I}...E.
0010  00 3c 00 00 40 00 40 06 1c 84 ac 10 63 0c ac 10   .<..@.@.....c...
0020  63 0b 13 89 b8 3e 71 60 35 4e 90 1f aa ae a0 12   c....>q`5N......
0030  71 20 52 33 00 00 02 04 05 b4 04 02 08 0a 00 0f   q R3............
0040  e6 9f 00 10 d2 60 01 03 03 07                     .....`....

# tshark -r captura.pcap -x -Y "frame.number==9"
Running as user "root" and group "root". This could be dangerous.
0000  94 de 80 49 7d 9f 14 da e9 99 a1 bc 08 00 45 00   ...I}.........E.
0010  00 34 78 ce 40 00 40 06 a3 bd ac 10 63 0b ac 10   .4x.@.@.....c...
0020  63 0c b8 3e 13 89 90 1f aa ae 71 60 35 4f 80 10   c..>......q`5O..
0030  00 e5 f1 3a 00 00 01 01 08 0a 00 10 d2 60 00 0f   ...:.........`..
0040  e6 9f                                             ..



Abrir dos servidores en dos puertos distintos
# iperf -s -i 2 -p 5005 
# iperf -s -i 2 -p 50015


Observar como quedan esos puertos abiertos con netstat

#netstat -utlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:5005            0.0.0.0:*               LISTEN      2631/iperf          
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1407/cupsd          
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      707/postgres        
tcp        0      0 0.0.0.0:50015           0.0.0.0:*               LISTEN      2634/iperf          
tcp6       0      0 ::1:631                 :::*                    LISTEN      1407/cupsd          
tcp6       0      0 ::1:5432                :::*                    LISTEN      707/postgres        
udp        0      0 0.0.0.0:64437           0.0.0.0:*                           760/dhclient        
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           601/avahi-daemon: r 
udp        0      0 0.0.0.0:50851           0.0.0.0:*                           601/avahi-daemon: r 
udp        0      0 0.0.0.0:68              0.0.0.0:*                           760/dhclient        
udp        0      0 127.0.0.1:323           0.0.0.0:*                           623/chronyd         
udp6       0      0 :::5353                 :::*                                601/avahi-daemon: r 
udp6       0      0 :::52698                :::*                                601/avahi-daemon: r 
udp6       0      0 :::63344                :::*                                760/dhclient        
udp6       0      0 ::1:323                 :::*                                623/chronyd   

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto
# iperf -s 
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 172.16.99.12 port 5001 connected with 172.16.99.11 port 47172
[  5] local 172.16.99.12 port 5001 connected with 172.16.99.12 port 48310
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0-60.0 sec   674 MBytes  94.1 Mbits/sec
[  5]  0.0-60.0 sec   168 GBytes  24.1 Gbits/sec


# iperf -t 60 -c 172.16.99.12 
------------------------------------------------------------
Client connecting to 172.16.99.12, TCP port 5001
TCP window size: 2.50 MByte (default)
------------------------------------------------------------
[  3] local 172.16.99.12 port 48310 connected with 172.16.99.12 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-60.0 sec   168 GBytes  24.1 Gbits/sec



Mientras tanto con netstat mirar conexiones abiertas

	por el mismo puerto 5001 esta escuchando a las dos conexiones.
netstat -utlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      2681/iperf          
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1407/cupsd          
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      707/postgres        
tcp6       0      0 ::1:631                 :::*                    LISTEN      1407/cupsd          
tcp6       0      0 ::1:5432                :::*                    LISTEN      707/postgres        
udp        0      0 0.0.0.0:64437           0.0.0.0:*                           760/dhclient        
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           601/avahi-daemon: r 
udp        0      0 0.0.0.0:50851           0.0.0.0:*                           601/avahi-daemon: r 
udp        0      0 0.0.0.0:68              0.0.0.0:*                           760/dhclient        
udp        0      0 127.0.0.1:323           0.0.0.0:*                           623/chronyd         
udp6       0      0 :::5353                 :::*                                601/avahi-daemon: r 
udp6       0      0 :::52698                :::*                                601/avahi-daemon: r 
udp6       0      0 :::63344                :::*                                760/dhclient        
udp6       0      0 ::1:323                 :::*                                623/chronyd         

