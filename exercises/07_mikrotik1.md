EJERCICIO MIKROTIK 1.

OBJETIVO: FAMILIARIZARSE CON LAS OPERACIONES BÁSICAS EN UN ROUTER MIKROTIK

#1. Entrar al router con telnet, ssh y winbox

descargamos el programa telnet
	#dnf -y install telnet
	#telnet 192.168.88.1
	#ssh admin@192.168.88.1
	

#2. Como resetear el router

	#system reset-configuration
	
	usar el boton de reset 5 secons en cuanto parpadee soltar el boton 


#3. Cambiar nombre del dispositivo, password 

	> system identity set name=infMKT12
	> system identity set password=clase
	
	o
	
	> system identity set name=infMKT12  password=clase
	
	
#4. Esquema de configuración de puertos inicial
	
	[admin@infMKT12] > interface print 
	Flags: D - dynamic, X - disabled, R - running, S - slave 
	 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
	 0     ether1                              ether            1500  1598       2028 6C:3B:6B:C3:89:EE
	 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:C3:89:EF
	 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:C3:89:F0
	 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:C3:89:F1
	 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:C3:89:F2
	 5  R  ;;; defconf
		   bridge                              bridge           1500  1598            6C:3B:6B:C3:89:EF
	             

#5. Cambiar nombres de puertos y deshacer configuraciones iniciales

[admin@infMKT12] > interface print
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:C3:89:EE
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:C3:89:EF
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:C3:89:F0
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:C3:89:F1
 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:C3:89:F2
 5  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:C3:89:EF
[admin@infMKT12] > interface set name=eth1prova
numbers: 0
[admin@infMKT12] > interface print
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1prova                           ether            1500  1598       2028 6C:3B:6B:C3:89:EE
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:C3:89:EF
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:C3:89:F0
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:C3:89:F1
 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:C3:89:F2
 5  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:C3:89:EF


#6. Backup y restauración de configuraciones

[admin@infMKT12] > system backup  

.. -- go up to system
load -- Loads a previously saved backup
save -- Makes a backup of the current configuration



> system backup save name=nombre_backup

```
https://roure.act.uji.es/wiki/public/guifinet/cursoinstaladoresguifi2011/routeros/start

pagina de documentacion
```
