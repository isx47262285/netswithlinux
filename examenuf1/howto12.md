# ejercio 1
```
	[root@j12 ~]# systemctl stop NetworkManager
	[root@j12 ~]# dhclient -r
	[root@j12 ~]# ip a f dev enp2s0
	[root@j12 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
```

### procedemos a cambiar las ips 
```
]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
[root@j12 ~]# ip a a 10.8.8.112/16 dev enp2s0
[root@j12 ~]# ip a a 10.7.12.100/24 dev enp2s0
[root@j12 ~]# ip a 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.112/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.7.12.100/24 scope global enp2s0
       valid_lft forever preferred_lft forever
3: enp0s29u1u3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
```

#hacemos ping al profe  10.8.1.1

```
[root@j12 ~]# ping 10.8.1.1 
PING 10.8.1.1 (10.8.1.1) 56(84) bytes of data.
64 bytes from 10.8.1.1: icmp_seq=1 ttl=64 time=0.324 ms
64 bytes from 10.8.1.1: icmp_seq=2 ttl=64 time=0.240 ms
64 bytes from 10.8.1.1: icmp_seq=3 ttl=64 time=0.229 ms
64 bytes from 10.8.1.1: icmp_seq=4 ttl=64 time=0.224 ms
^C
--- 10.8.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3000ms
rtt min/avg/max/mdev = 0.224/0.254/0.324/0.042 ms


[root@j12 ~]# ping 10.7.12.1 
PING 10.7.12.1 (10.7.12.1) 56(84) bytes of data.
64 bytes from 10.7.12.1: icmp_seq=1 ttl=64 time=0.335 ms
64 bytes from 10.7.12.1: icmp_seq=2 ttl=64 time=0.239 ms
64 bytes from 10.7.12.1: icmp_seq=3 ttl=64 time=0.207 ms
64 bytes from 10.7.12.1: icmp_seq=4 ttl=64 time=0.168 ms
^C
--- 10.7.12.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3000ms
rtt min/avg/max/mdev = 0.168/0.237/0.335/0.062 ms
[root@j12 ~]# 
```
#ejercicio 2
```
[root@j12 ~]# ip a a 10.9.9.212/24 dev enp2s0
[root@j12 ~]# ip r a 10.6.6.0/16 via 10.9.9.1

```
ponemos una ruta estatica y hacemos ping!

vemos q funciona !
```
[root@j12 ~]# ip r a 10.6.6.0/24 via 10.9.9.1
[root@j12 ~]# ping 10.6.6.1
PING 10.6.6.1 (10.6.6.1) 56(84) bytes of data.
64 bytes from 10.6.6.1: icmp_seq=1 ttl=64 time=0.187 ms
64 bytes from 10.6.6.1: icmp_seq=2 ttl=64 time=0.196 ms
64 bytes from 10.6.6.1: icmp_seq=3 ttl=64 time=0.163 ms
64 bytes from 10.6.6.1: icmp_seq=4 ttl=64 time=0.192 ms
64 bytes from 10.6.6.1: icmp_seq=5 ttl=64 time=0.196 ms
^C
--- 10.6.6.1 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 3999ms
rtt min/avg/max/mdev = 0.163/0.186/0.196/0.021 ms
[root@j12 ~]# ip r
10.6.6.0/24 via 10.9.9.1 dev enp2s0 
10.7.12.0/24 dev enp2s0  proto kernel  scope link  src 10.7.12.100 
10.8.0.0/16 dev enp2s0  proto kernel  scope link  src 10.8.8.112 
10.9.9.0/24 dev enp2s0  proto kernel  scope link  src 10.9.9.212 
[root@j12 ~]# 
```
#ejercicio 3
```
	[root@j12 ~]# tshark -i enp2s0 -c 2 -w  /tmp/ping.pcap icmp
	Running as user "root" and group "root". This could be dangerous.
	Capturing on 'enp2s0'
	2 
	[root@j12 ~]# tshark -r /tmp/ping.pcap 
	Running as user "root" and group "root". This could be dangerous.
	1 0.000000000   10.9.9.212 → 10.6.6.1     ICMP 98 Echo (ping) request  id=0x082c, seq=7/1792, ttl=64
	2 0.000177185     10.6.6.1 → 10.9.9.212   ICMP 98 Echo (ping) reply    id=0x082c, seq=7/1792, ttl=64 (request in 1)



	el ttl = 64 !!  es decir que el paquete ha llegado y responde el ping correctamente
```

#ejercico 4

```
[root@j12 ~]# ip link set enp0s29u1u3 down
[root@j12 ~]# ip link set name usb12 enp0s29u1u3
[root@j12 ~]# ip a 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.112/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.7.12.100/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.212/24 scope global enp2s0
       valid_lft forever preferred_lft forever
3: usb12: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
```


se ha cambiado el nombre de la interface

hemos cambiado la mac de la interficie usb12




```
[root@j12 ~]# ip link set usb12 up


[root@j12 ~]# ip link set enp0s29u1u4 down
[root@j12 ~]# ip link set name usb12 enp0s29u1u4
[root@j12 ~]# ip link set address 44:44:44:00:00:12 usb12
[root@j12 ~]# ip a a 10.5.5.112/24 dev usb12
[root@j12 ~]# ip link set usb12 up
[root@j12 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.112/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.7.12.100/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.212/24 scope global enp2s0
       valid_lft forever preferred_lft forever
9: usb12: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 44:44:44:00:00:12 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.112/24 scope global usb12
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:12/64 scope link 
       valid_lft forever preferred_lft forever
```
hacemos ping al 10.5.5.1 y responde 
```
	[root@j12 ~]# ping 10.5.5.1
	PING 10.5.5.1 (10.5.5.1) 56(84) bytes of data.
	64 bytes from 10.5.5.1: icmp_seq=1 ttl=64 time=0.663 ms
	64 bytes from 10.5.5.1: icmp_seq=2 ttl=64 time=0.684 ms
	64 bytes from 10.5.5.1: icmp_seq=3 ttl=64 time=0.679 ms
	64 bytes from 10.5.5.1: icmp_seq=4 ttl=64 time=0.678 ms
	^C
	--- 10.5.5.1 ping statistics ---
	4 packets transmitted, 4 received, 0% packet loss, time 3000ms
	rtt min/avg/max/mdev = 0.663/0.676/0.684/0.007 ms
```
#ejercico 5

```
[root@j12 ~]# ip a a 172.17.0.1/16 dev usb12
[root@j12 ~]# ip a 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.112/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.7.12.100/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.212/24 scope global enp2s0
       valid_lft forever preferred_lft forever
10: usb12: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:12 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global usb12
       valid_lft forever preferred_lft forever

```
``
[root@j12 ~]# echo 1 > /proc/sys/net/ipv4/ip_forward
[root@j12 ~]# iptables -t nat -A POSTROUTING -o enp2s0 -j MASQUERADE
[root@j12 ~]# ip link set usb12 up
[root@j12 ~]# ip a 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
10: usb12: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:12 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global usb12
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:12/64 scope link 
       valid_lft forever preferred_lft forever
```      
comprovamos que tenemos ruta de salida por usb12

```    
[root@j12 ~]# ip r s
172.17.0.0/16 dev usb12  proto kernel  scope link  src 172.17.0.1 linkdown 
```

comprovamos el masquerade 

[root@j12 ~]# iptables -t  nat -L
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         

Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
MASQUERADE  all  --  anywhere             anywhere            
[root@j12 ~]# 

```
#ejercicios 6
```
[root@j12 ~]# dhclient
[root@j12 ~]# host papua.go.id
papua.go.id has address 180.250.209.166
papua.go.id mail is handled by 100 us2.mx1.mailhostbox.com.
papua.go.id mail is handled by 200 us2.mx2.mailhostbox.com.
papua.go.id mail is handled by 300 us2.mx3.mailhostbox.com.
[root@j12 ~]# 
```
miramos los saltos de la ip 

```
[root@j12 ~]# traceroute  180.250.209.166 
traceroute to 180.250.209.166 (180.250.209.166), 30 hops max, 60 byte packets
 1  gandhi-vm.informatica.escoladeltreball.org (192.168.0.5)  0.312 ms  0.250 ms  0.219 ms
 2  sw-l3.escoladeltreball.org (10.1.1.199)  3.489 ms  3.549 ms  3.568 ms
 3  10.10.1.4 (10.10.1.4)  0.378 ms  0.338 ms  0.382 ms
 4  115.red-80-58-67.staticip.rima-tde.net (80.58.67.115)  1.985 ms  2.090 ms  2.110 ms
 5  77.red-80-58-80.staticip.rima-tde.net (80.58.80.77)  7.265 ms  7.268 ms  7.242 ms
 6  46.red-80-58-81.staticip.rima-tde.net (80.58.81.46)  4.908 ms  3.769 ms  3.751 ms
 7  et3-0-0-400-grtbcntb1.net.telefonicaglobalsolutions.com (94.142.103.177)  1.733 ms  1.841 ms  1.818 ms
 8  xe1-0-5-0-grtpareq1.net.telefonicaglobalsolutions.com (94.142.119.177)  26.430 ms xe6-0-1-0-grtparix3.net.telefonicaglobalsolutions.com (94.142.121.25)  37.027 ms xe0-1-2-0-grtlontc2.net.telefonicaglobalsolutions.com (176.52.248.18)  36.106 ms
 9  xe-2-0-1-0-grtnycpt2.red.telefonica-wholesale.net (94.142.126.61)  99.537 ms xe-3-1-0-0-grtnycpt2.red.telefonica-wholesale.net (94.142.126.65)  104.427 ms xe4-1-1-0-grtnycpt2.net.telefonicaglobalsolutions.com (94.142.117.200)  106.277 ms
10  xe5-1-0-0-grtnycpt3.net.telefonicaglobalsolutions.com (94.142.127.133)  107.395 ms xe4-1-3-0-grtnycpt2.net.telefonicaglobalsolutions.com (213.140.35.194)  104.123 ms xe1-1-8-0-grtpaopx3.net.telefonicaglobalsolutions.com (84.16.15.148)  182.517 ms
11  xe1-0-0-0-grtpaopx3.net.telefonicaglobalsolutions.com (94.142.117.196)  269.539 ms xe2-0-4-0-grtpaopx3.net.telefonicaglobalsolutions.com (176.52.255.241)  224.093 ms 213.140.49.5 (213.140.49.5)  226.989 ms
12  * ae-15.r01.snjsca04.us.bb.gin.ntt.net (129.250.5.33)  339.944 ms *
13  ae-1.r22.snjsca04.us.bb.gin.ntt.net (129.250.3.26)  179.163 ms ae-15.r01.snjsca04.us.bb.gin.ntt.net (129.250.5.33)  344.054 ms  343.124 ms
14  ae-2.r20.sngpsi05.sg.bb.gin.ntt.net (129.250.3.49)  342.352 ms  339.860 ms ae-1.r22.snjsca04.us.bb.gin.ntt.net (129.250.3.26)  181.616 ms
15  ae-2.r00.sngpsi02.sg.bb.gin.ntt.net (129.250.3.147)  352.274 ms  344.072 ms  350.436 ms
16  ae-2.r00.sngpsi02.sg.bb.gin.ntt.net (129.250.3.147)  347.081 ms 116.51.26.38 (116.51.26.38)  342.902 ms ae-2.r00.sngpsi02.sg.bb.gin.ntt.net (129.250.3.147)  352.157 ms
17  180.240.204.74 (180.240.204.74)  352.738 ms 116.51.26.38 (116.51.26.38)  344.179 ms 180.240.204.74 (180.240.204.74)  352.038 ms
18  * * 180.240.204.74 (180.240.204.74)  365.044 ms
19  180.240.193.205 (180.240.193.205)  344.352 ms 180.240.204.82 (180.240.204.82)  359.650 ms 180.240.204.78 (180.240.204.78)  354.994 ms
20  61.94.117.125 (61.94.117.125)  346.389 ms 180.250.209.161 (180.250.209.161)  479.405 ms 61.94.117.125 (61.94.117.125)  360.000 ms
21  180.250.209.166 (180.250.209.166)  444.616 ms !X 180.250.209.161 (180.250.209.161)  474.518 ms  474.251 ms



```
hay 21 saltos  y esto concuerda con el geoip que nos dice que la ip destino esta en indonesia 
```
[root@j12 ~]# geoiplookup 180.250.209.166
GeoIP Country Edition: ID, Indonesia
GeoIP City Edition, Rev 1: ID, N/A, N/A, N/A, N/A, -6.175000, 106.828598, 0, 0
GeoIP ASNum Edition: AS17974 PT Telekomunikasi Indonesia
[root@j12 ~]# 
```
#ejercicio 7
```
[root@j12 ~]# netstat -utanpl
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      879/rpcbind         
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1477/cupsd          
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      716/postgres        
tcp        0      0 0.0.0.0:48859           0.0.0.0:*               LISTEN      877/rpc.statd       
tcp        0      0 192.168.3.12:697        192.168.0.10:2049       ESTABLISHED -                   
tcp6       0      0 :::111                  :::*                    LISTEN      879/rpcbind         
tcp6       0      0 :::51349                :::*                    LISTEN      877/rpc.statd       
tcp6       0      0 ::1:631                 :::*                    LISTEN      1477/cupsd          
tcp6       0      0 ::1:5432                :::*                    LISTEN      716/postgres        
udp        0      0 192.168.3.12:39801      180.250.209.166:33503   ESTABLISHED 2550/traceroute     
udp        0   1536 0.0.0.0:5353            0.0.0.0:*                           628/avahi-daemon: r 
udp        0      0 0.0.0.0:19713           0.0.0.0:*                           2471/dhclient       
udp        0      0 0.0.0.0:32063           0.0.0.0:*                           756/dhclient        
udp        0      0 0.0.0.0:56658           0.0.0.0:*                           628/avahi-daemon: r 
udp        0      0 192.168.3.12:46525      180.250.209.166:33505   ESTABLISHED 2550/traceroute     
udp        0      0 0.0.0.0:68              0.0.0.0:*                           2471/dhclient       
udp        0      0 0.0.0.0:68              0.0.0.0:*                           756/dhclient        
udp        0      0 0.0.0.0:111             0.0.0.0:*                           879/rpcbind         
udp        0      0 192.168.3.12:39062      180.250.209.166:33506   ESTABLISHED 2550/traceroute     
udp        0      0 192.168.3.12:35074      180.250.209.166:33504   ESTABLISHED 2550/traceroute     
udp        0      0 127.0.0.1:323           0.0.0.0:*                           609/chronyd         
udp        0      0 0.0.0.0:630             0.0.0.0:*                           879/rpcbind         
udp        0      0 127.0.0.1:659           0.0.0.0:*                           877/rpc.statd       
udp        0      0 192.168.3.12:51880      192.168.0.10:53         ESTABLISHED 2550/traceroute     
udp        0      0 0.0.0.0:54029           0.0.0.0:*                           877/rpc.statd       
udp        0      0 192.168.3.12:35619      180.250.209.166:33502   ESTABLISHED 2550/traceroute     
udp6       0      0 :::56145                :::*                                877/rpc.statd       
udp6       0      0 :::60627                :::*                                628/avahi-daemon: r 
udp6       0      0 :::5353                 :::*                                628/avahi-daemon: r 
udp6       0      0 :::5400                 :::*                                756/dhclient        
udp6       0      0 ::1:42499               ::1:42499               ESTABLISHED 716/postgres        
udp6       0      0 :::16483                :::*                                2471/dhclient       
udp6       0      0 :::111                  :::*                                879/rpcbind         
udp6       0      0 ::1:323                 :::*                                609/chronyd         
udp6       0      0 :::630                  :::*                                879/rpcbind         

```

cups escucha en el puerto 1477.
```
[root@j12 ~]# netstat -utanp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      879/rpcbind         
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1477/cupsd          
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      716/postgres        
tcp        0      0 0.0.0.0:48859           0.0.0.0:*               LISTEN      877/rpc.statd       
tcp        0      0 192.168.3.12:35342      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:42378      185.33.222.93:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35316      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:36508      54.77.220.54:80         ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:46928      216.58.211.206:443      ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35346      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:41288      83.247.129.60:80        TIME_WAIT   -                   
tcp        0      0 192.168.3.12:59730      52.211.182.185:80       TIME_WAIT   -                   
tcp        0      0 192.168.3.12:35344      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:56210      82.194.77.43:80         ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:56242      82.194.77.43:80         ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:49202      216.58.211.195:80       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35324      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:34638      104.244.43.145:443      ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:55456      185.29.134.193:80       TIME_WAIT   -                   
tcp        0      0 192.168.3.12:45562      104.244.42.72:443       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:46862      216.58.211.206:443      TIME_WAIT   -                   
tcp        0      0 192.168.3.12:41312      83.247.129.60:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:46942      216.58.211.206:443      ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:41450      199.96.57.6:80          ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:56218      82.194.77.43:80         ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:51090      199.96.57.6:443         ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35318      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:42620      185.86.139.19:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35338      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:59732      52.211.182.185:80       TIME_WAIT   -                   
tcp        0      0 192.168.3.12:35326      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:51162      216.58.211.202:80       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:33856      136.243.51.231:80       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:52682      136.243.54.217:80       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:58098      216.58.210.138:443      ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:42618      185.86.139.19:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:51092      199.96.57.6:443         ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35340      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:35354      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:697        192.168.0.10:2049       ESTABLISHED -                   
tcp        0      0 192.168.3.12:55466      185.29.134.193:80       TIME_WAIT   -                   
tcp        0      0 192.168.3.12:35320      52.218.64.114:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:49200      216.58.211.195:80       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:49204      216.58.211.195:80       ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:41316      83.247.129.60:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:42380      185.33.222.93:80        ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:59072      216.58.214.162:443      TIME_WAIT   -                   
tcp        0      0 192.168.3.12:34632      104.244.43.145:443      ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:41448      199.96.57.6:80          ESTABLISHED 2594/firefox        
tcp        0      0 192.168.3.12:51160      216.58.211.202:80       ESTABLISHED 2594/firefox        
tcp6       0      0 :::111                  :::*                    LISTEN      879/rpcbind         
tcp6       0      0 :::51349                :::*                    LISTEN      877/rpc.statd       
tcp6       0      0 ::1:631                 :::*                    LISTEN      1477/cupsd          
tcp6       0      0 ::1:5432                :::*                    LISTEN      716/postgres        
udp        0      0 192.168.3.12:43874      158.227.98.15:123       ESTABLISHED 609/chronyd         
udp        0   1536 0.0.0.0:5353            0.0.0.0:*                           628/avahi-daemon: r 
udp        0      0 0.0.0.0:19713           0.0.0.0:*                           2471/dhclient       
udp        0      0 0.0.0.0:32063           0.0.0.0:*                           756/dhclient        
udp        0      0 0.0.0.0:56658           0.0.0.0:*                           628/avahi-daemon: r 
udp        0      0 0.0.0.0:68              0.0.0.0:*                           2471/dhclient       
udp        0      0 0.0.0.0:68              0.0.0.0:*                           756/dhclient        
udp        0      0 0.0.0.0:111             0.0.0.0:*                           879/rpcbind         
udp        0      0 127.0.0.1:323           0.0.0.0:*                           609/chronyd         
udp        0      0 0.0.0.0:630             0.0.0.0:*                           879/rpcbind         
udp        0      0 127.0.0.1:659           0.0.0.0:*                           877/rpc.statd       
udp        0      0 0.0.0.0:54029           0.0.0.0:*                           877/rpc.statd       
udp6       0      0 :::56145                :::*                                877/rpc.statd       
udp6       0      0 :::60627                :::*                                628/avahi-daemon: r 
udp6       0      0 :::5353                 :::*                                628/avahi-daemon: r 
udp6       0      0 :::5400                 :::*                                756/dhclient        
udp6       0      0 ::1:42499               ::1:42499               ESTABLISHED 716/postgres        
udp6       0      0 :::16483                :::*                                2471/dhclient       
udp6       0      0 :::111                  :::*                                879/rpcbind         
udp6       0      0 ::1:323                 :::*                                609/chronyd         
udp6       0      0 :::630                  :::*                                879/rpcbind         
[root@j12 ~]# 
```
